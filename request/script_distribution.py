import mysql.connector
import base64
import sys
import matplotlib.pyplot as plt
import json
from io import BytesIO
import math
import random


mydb = mysql.connector.connect(host='localhost', user='StuData', passwd='019990625',
                                      database='StuData')

build=sys.argv[1]
#cls=sys.argv[2]
build=str(base64.b64decode(build),'utf-8')
#cls=str(base64.b64decode(cls),'utf-8')
# print(build)
b=build[:2]
c=build[2:]
# print(b)
# print(c)
search_set = (b, c)
# print(search_set)

cursor = mydb.cursor()
sql = "select bbox from BoxInf where Building=%s and Classes=%s"
cursor.execute(sql, search_set)
result = cursor.fetchall()

res=result[0][0]
# print(res)
text = json.loads(res)
text=text['data']
# print(text)
numOfPeople=len(text)
# print(numOfPeople)
x=[]
y=[]


for i in range(numOfPeople):
    x.append(text[i]['bbox'][0])
    y.append(text[i]['bbox'][1])
    if text[i]['is_too_close']== True:
        plt.scatter(x[i], y[i], c='r')
    else:
        plt.scatter(x[i], y[i], c='g')
#
#
# print(x)
# print(y)
plt.xlim(xmax=int(max(x)) + 50, xmin=0)
plt.ylim(ymax=1080, ymin=0)

plt.axis('off')
ax = plt.gca()  # 获取到当前坐标轴信息
ax.xaxis.set_ticks_position('top')  # 将X坐标轴移到上面
ax.invert_yaxis()  # 反转Y坐标轴
# plt.show()
nums = math.floor(1e5 * random.random())
print(nums)

plt.savefig("/www/wwwroot/web/request//tempImg/"+str(nums)+".jpg",facecolor='cornsilk')
# save_file = BytesIo()
# plt.savefig(save_file, format='png')
# save_file_base64 = base64.b64encode(save_file.getvalue()).decode('utf8')
# print(save_file_base64)
# print("done")
