<?php
$servername = "localhost";
$username = "StuData";
$password = "019990625";
$dbname = "StuData";

$conn = new mysqli($servername, $username, $password, $dbname);


// 检测连接
if ($conn->connect_error) {
    die("连接失败: " . $conn->connect_error);
}

$type = $_GET['type'];
$buildingName = $_GET['build'];
$cls = $_GET['class'];
$key = $_GET['key'];
if ($type == 'AllBuild') {
    $sql = "SELECT * FROM building";
    $result = $conn->query($sql);
    $arr = array();
    while ($row = mysqli_fetch_array($result)) {
        $count = count($row);//不能在循环语句中，由于每次删除 row数组长度都减小
        for ($i = 0; $i < $count; $i++) {
            unset($row[$i]);//删除冗余数据
        }
        $r = array('BuildName' => $row['Building'], 'MaxNum' => (float)$row['MaxNum'], 'Num' => (float)$row['num'], 'Percentage' => (float)$row['Percentage']);
        // if($row['Building']=='LC'){
        //     $r=array('BuildName'=>$row['Building'],'MaxNum'=>(float)$row['MaxNum'],'Num'=>(float)$row['num'],'Percentage'=>(float)$row['Percentage']);
        //     $res=array('LC'=>$r);
        // }
        // elseif($row['Building']=='XD'){
        //     $r=array('MaxNum'=>(float)$row['MaxNum'],'Num'=>(float)$row['num'],'Percentage'=>(float)$row['Percentage']);
        //     $res=array('XD'=>$r);

        // }
        // elseif($row['Building']=='ZG'){
        //     $r=array('MaxNum'=>(float)$row['MaxNum'],'Num'=>(float)$row['num'],'Percentage'=>(float)$row['Percentage']);
        //     $res=array('ZG'=>$r);

        // }
        // elseif($row['Building']=='ZM'){
        //     $r=array('MaxNum'=>(float)$row['MaxNum'],'Num'=>(float)$row['num'],'Percentage'=>(float)$row['Percentage']);
        //     $res=array('ZM'=>$r);

        // }
        array_push($arr, $r);

    }

    echo json_encode($arr);
} else if ($type == 'OneBuild') {
    $sql = "SELECT * FROM classes where Building='$buildingName'";
    $result = $conn->query($sql);
    $arr = array();
    $twoD = array();
    while ($row = mysqli_fetch_array($result)) {
        $count = count($row);//不能在循环语句中，由于每次删除 row数组长度都减小
        for ($i = 0; $i < $count; $i++) {
            unset($row[$i]);//删除冗余数据
        }
        $Class = $row['Classes'];
        $r = array('Class' => $Class, 'MaxNum' => (float)$row['MaxNum'], 'Num' => (float)$row['num'], 'Percentage' => (float)$row['Percentage']);
        //$c=array($Class=>$r);
        array_push($twoD, $r);
        //$res['Class']=array($r);
        //$res=array('BuildName'=>$row['Building'],'Class'=>$c);
    }
    $arr['BuildName'] = $buildingName;
    $arr['Class'] = $twoD;
    //array_push($arr,$res);
    echo json_encode($arr);
} else if ($type == 'AllClass') {
    $sql = "SELECT * FROM classes order by Percentage";
    $result = $conn->query($sql);
    $arr = array();
    while ($row = mysqli_fetch_array($result)) {
        $count = count($row);//不能在循环语句中，由于每次删除 row数组长度都减小
        for ($i = 0; $i < $count; $i++) {
            unset($row[$i]);//删除冗余数据
        }
        $r = array('BuildName' => $row['Building'], 'Class' => $row['Classes'], 'MaxNum' => (float)$row['MaxNum'], 'Num' => (float)$row['num'], 'Percentage' => (float)$row['Percentage']);
        array_push($arr, $r);

    }
    echo json_encode($arr);
} else if ($type == 'OneClass') {
    $sql = "SELECT * FROM time where Building='$buildingName' and Class='$cls'";
    $result = $conn->query($sql);
    $arr = array();
    $twoD = array();
    while ($row = mysqli_fetch_array($result)) {
        $count = count($row);//不能在循环语句中，由于每次删除 row数组长度都减小
        for ($i = 0; $i < $count; $i++) {
            unset($row[$i]);//删除冗余数据
        }
        //$da=array('time'=>$row['time'],'num'=>$row['num']);
        $r = array('time' => $row['time'], 'num' => $row['num']);
        array_push($twoD, $r);

    }
    $arr['BuildName'] = $buildingName;
    $arr['Class'] = $cls;
    $arr['data'] = $twoD;
    echo json_encode($arr);
} else if ($type == 'Search') {
    $lfront = mb_strpos($key, "^^");
    if ($lfront != FALSE) {
        $certain = mb_substr($key, 0, $lfront);

        //echo (mb_substr($certain,6,1));

        $arr = array();

        if (mb_strpos($certain, "@@") == FALSE) {
            array_push($arr, $certain);
        } else {
            $last = 0;
            for ($i = 0; $i < mb_strlen($certain); $i++) {
                if (mb_substr($certain, $i, 1) == '@') {
                    array_push($arr, mb_substr($certain, $last, $i - $last));
                    $i++;
                    $last = $i + 1;
                }
            }
            array_push($arr, mb_substr($certain, $last, mb_strlen($certain) - $last));
        }
        //print_r($arr);


        $buil = array();
        $ccla = array();
        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$i] == '知' || $arr[$i] == '知明' || $arr[$i] == '明') {
                array_push($buil, 'ZM');
            } else if ($arr[$i] == '笃' || $arr[$i] == '行' || $arr[$i] == '笃行') {
                array_push($buil, 'XD');
            } else if ($arr[$i] == '立' || $arr[$i] == '诚' || $arr[$i] == '立诚') {
                array_push($buil, 'LC');
            } else if ($arr[$i] == '致' || $arr[$i] == '广' || $arr[$i] == '致广') {
                array_push($buil, 'ZG');
            } else {
                $temp = $arr[$i];
                $checksql = "SELECT * FROM classes where Classes like '%$temp%'";
                $res = $conn->query($checksql);
                $rr = mysqli_num_rows($res);
                if ($rr > 0) {
                    array_push($ccla, $arr[$i]);
                }
            }
        }
        $buil = array_values(array_unique($buil));
        $ccla = array_values(array_unique($ccla));

        if (count($buil) != 0) {
            $sqlbuil = $buil[0];
        }

        // print_r($buil);
        // print_r($ccla);


        $sqlclass = "";
        for ($i = 0; $i < count($ccla); $i++) {
            $sqlc = $ccla[$i];
            if (count($ccla) == 1) {
                $sqlclass = $sqlclass . "(Classes like '%$sqlc%')";
            } else {
                if ($i == 0) {
                    $sqlclass = $sqlclass . "(Classes like '%$sqlc%' and ";
                } else if ($i == count($ccla) - 1) {
                    $sqlclass = $sqlclass . "Classes like '%$sqlc%')";
                } else {
                    $sqlclass = $sqlclass . "Classes like '%$sqlc%' and ";
                }
            }

        }

        // $arr_un=array();
        // if($lfront+2<mb_strlen($key)){
        //     $uncertain=mb_substr($lfront+2);
        //     $last=$lfront+2;
        //     if(mb_strpos($uncertain,"@@")==FALSE){
        //         array_push($arr_un,$uncertain);
        //     }
        //     else{
        //         $last=0;
        //         for($i=0;$i<mb_strlen($uncertain);$i++){
        //             if(mb_substr($uncertain,$i,1)=='@'){
        //                 array_push($arr_un,mb_substr($uncertain,$last,$i-$last));
        //                 $i++;
        //                 $last=$i+1;
        //             }
        //         }
        //         array_push($arr_un,mb_substr($uncertain,$last,mb_strlen($uncertain)-$last));
        //     }
        // }


        if (count($buil) == 0) {
            $sql = "SELECT * FROM classes where $sqlclass";
        } else if (count($ccla) == 0) {
            $sql = "SELECT * FROM classes where Building like '$sqlbuil'";
        } else {
            $sql = "SELECT * FROM classes where Building like '$sqlbuil' and $sqlclass";
        }


        //echo $sql;
        $result = $conn->query($sql);
        //$r=array();
        $twoD = array();
        if (count($buil) == 0 && count($ccla) == 0) {
            $fin = array("Result" => $twoD);
            echo json_encode($fin);
        } else {
            while ($row = mysqli_fetch_array($result)) {
                $count = count($row);//不能在循环语句中，由于每次删除 row数组长度都减小
                for ($i = 0; $i < $count; $i++) {
                    unset($row[$i]);//删除冗余数据 
                }
                $r = array('BuildName' => $row['Building'], 'Class' => $row['Classes'], 'MaxNum' => (int)$row['MaxNum'], 'Num' => (int)$row['num'], 'Percentage' => (float)$row['Percentage']);
                array_push($twoD, $r);
                //array_push($r,$row);

            }
            $fin = array("Result" => $twoD);
            echo json_encode($fin);
        }

    }
}


function ipaddress()
{
    global $ip;
    if (getenv("HTTP_CLIENT_IP"))
        $ip = getenv("HTTP_CLIENT_IP");
    else if (getenv("HTTP_X_FORWARDED_FOR"))
        $ip = getenv("HTTP_X_FORWARDED_FOR");
    else if (getenv("REMOTE_ADDR"))
        $ip = getenv("REMOTE_ADDR");
    else
        $ip = "Unknow";
    return $ip;
}

$ip = ipaddress();
$d = date('Y-m-d H:i:s');
mysqli_query($conn, "INSERT INTO visit VALUES('$ip','$d')");
//echo $ip;

//echo $d;
$conn->close();
?>