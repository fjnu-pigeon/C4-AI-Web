<?php
    $servername="localhost";
    $username = "StuData";
    $password = "019990625";
    $dbname = "StuData";

    $conn = new mysqli($servername, $username, $password, $dbname);
    
    
    // 检测连接
    if ($conn->connect_error) {
        die("连接失败: " . $conn->connect_error);
    }


    $sql = "SELECT bbox FROM boxinf where Building='LC' and Classes='1-101'";
    $result = $conn->query($sql);
    $arr = array(); 
    while($row = mysqli_fetch_array($result)) { 
        $count=count($row);//不能在循环语句中，由于每次删除 row数组长度都减小 
        for($i=0;$i<$count;$i++){ 
            unset($row[$i]);//删除冗余数据 
        }
        array_push($arr,$row);  
    }
    $ret=json_decode($arr[0]['bbox'],true); 
    
    $numOfPeople=count($ret);
    $x=array();
    $y=array();
    $isColse=array();
    for($i=0;$i<$numOfPeople;$i++){
        array_push($x,(int)($ret[$i]['bbox'][0]/10));
        array_push($y,(int)($ret[$i]['bbox'][1]/10));
        array_push($isColse,$ret[$i]['is_too_close']);    
    }
    
  

    require_once("bootstrap.php");

	use pChart\pColor;
	use pChart\pDraw;
	use pChart\pCharts;
    
    $myPicture = new pDraw(1080,800);
	$myPicture->myData->addPoints($y,"Serie1");
	$myPicture->myData->setSerieDescription("Serie1","Serie 1");
	$myPicture->myData->setSerieOnAxis("Serie1",0);
	
	$myPicture->myData->addPoints($x,"Absissa");
	$myPicture->myData->setAbscissa("Absissa");
	
	$myPicture->myData->setAxisProperties(0, ["Name" => "1st axis", "Position" => AXIS_POSITION_LEFT, "Unit" => ""]);
	$Settings = ['Color'=>new pColor(0), 'Dash'=>DIRECTION_VERTICAL, 'DashColor'=>new pColor(20)];
	$myPicture->drawFilledRectangle(0,0,1080,800,$Settings);
	$myPicture->setFontProperties(["FontName"=>"fonts/Cairo-Regular.ttf","FontSize"=>14]);
	$TextSettings = ['Align'=>TEXT_ALIGN_MIDDLEMIDDLE, 'Color'=>new pColor(255)];
	$myPicture->drawText(350,25,"My first pChart project",$TextSettings);
	
	$myPicture->setGraphArea(200,200,1050,700);
	$myPicture->setFontProperties(["Color"=> new pColor(0),"FontName"=>"fonts/Cairo-Regular.ttf","FontSize"=>7]);
	
	$Settings = ['Pos'=>SCALE_POS_TOPBOTTOM, 'Mode'=>SCALE_MODE_FLOATING, 'LabelingMethod'=>LABELING_DIFFERENT, 'GridColor'=>new pColor(7,7,7,50), 'TickColor'=>new pColor(0,0,0,50), 'LabelRotation'=>0, 'DrawXLines'=>VOID, 'DrawYLines'=>NONE];
	$myPicture->drawScale($Settings);
	
	$Config = ['PlotSize'=>3, 'PlotBorder'=>DIRECTION_VERTICAL, 'BorderSize'=>2];
	(new pCharts($myPicture))->drawPlotChart($Config);
	
	$myPicture->stroke();
    

?>