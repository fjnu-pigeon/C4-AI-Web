<?php   
/* CAT:Spline chart */

/* pChart library inclusions */
	require_once("bootstrap.php");

	use pChart\pColor;
	use pChart\pDraw;
	use pChart\pCharts;
	
	$servername="localhost";
    $username = "StuData";
    $password = "019990625";
    $dbname = "StuData";

    $conn = new mysqli($servername, $username, $password, $dbname);
    
    
    // 检测连接
    if ($conn->connect_error) {
        die("连接失败: " . $conn->connect_error);
    }

	$type=$_GET['type'];
    $buildingName=$_GET['build'];
    $cls=$_GET['class'];
	if($type=='OneClass'){
		$sql = "SELECT * FROM time where Building='$buildingName' and Class='$cls' order by time";
		$result = $conn->query($sql);
		$arr = array();
		$n=0;
		while($row = mysqli_fetch_array($result)) { 
			$n++;
			$count=count($row);//不能在循环语句中，由于每次删除 row数组长度都减小 
			for($i=0;$i<$count;$i++){ 
				unset($row[$i]);//删除冗余数据 
			}
			array_push($arr,$row);  
		}
		$res=array();
		for($i=0;$i<$n;$i++){
			$res[$arr[$i]['time']]=0;
		}
		for($j=0;$j<$n;$j++){
			$res[$arr[$j]['time']]+=$arr[$j]['num'];
		}
		//echo count($arr);
		//echo json_encode($res);
		$x=array();
		for($k=0;$k<count($arr);$k++){
			array_push($x,$arr[$k]['time']);
		}
		$x=array_values(array_unique($x));
		$y=array();
		for($m=0;$m<count($x);$m++){
			array_push($y,$res[$x[$m]]);
		}
		$nx=array();
		$ny=array();
		if(count($x)<=10){
			for($i=0;$i<count($x);$i++){
				$x[$i]=substr($x[$i],11,5);
			}
			if(count($x)>5){
				$cc=0;
				for($i=0;$i<count($x);$i++){
					if($cc!=2&&$i!=0){
						$x[$i]="";
						$cc++;
					}
					else{
						$cc=0;
					}
				}
			}
		}
		if(count($x)>10){
			$xstep=floor(count($x)/10);
			for($i=0;$i<count($x);$i+=$xstep){
				array_push($nx,$x[$i]);
			}
			$ystep=floor(count($y)/10);
			for($i=0;$i<count($y);$i+=$ystep){
				array_push($ny,$y[$i]);
			}
			
			for($i=0;$i<count($nx);$i++){
				$nx[$i]=substr($nx[$i],11,5);
			}
			$c=0;
			for($i=0;$i<count($nx);$i++){
				if($c!=2&&$i!=0){
					$nx[$i]="";
					$c++;
				}
				else{
					$c=0;
				}
			}
		}
		
		
		
		$myPicture = new pDraw(1080,800);

		$myPicture->myData->loadPalette([[253,184,19,100],[246,139,31,100],[241,112,34,100],[98,194,204,100],[228,246,248,100],[238,246,108,100]],TRUE);
		if(count($y)<=10){
			$myPicture->myData->addPoints($y,"Serie2");
		}
		else{
			$myPicture->myData->addPoints($ny,"Serie2");
		}
		$myPicture->myData->setSerieDescription("Serie2","Serie 2");
		$myPicture->myData->setSerieOnAxis("Serie2",0);
		if(count($x)<=10){
			$myPicture->myData->addPoints($x,"Absissa");
		}
		else{
			$myPicture->myData->addPoints($nx,"Absissa");
		}
		$myPicture->myData->setAbscissa("Absissa");

		$myPicture->myData->setAxisProperties(0, ["Name" => "", "Position" => AXIS_POSITION_LEFT, "Unit" => ""]);
		$Settings = ['Color'=>new pColor(130,249,255), 'Dash'=>DIRECTION_VERTICAL, 'DashColor'=>new pColor(150,255,255)];
		$myPicture->drawFilledRectangle(0,0,1080,800,$Settings);
		$Settings = ["StartColor"=> new pColor(251),"EndColor"=> new pColor(252,254,254)];
		$myPicture->drawGradientArea(0,0,1080,800,DIRECTION_VERTICAL,$Settings);

		$myPicture->setShadow(TRUE,["X"=>1,"Y"=>1,"Color"=>new pColor(50,50,50,20)]);

		$myPicture->setShadow(FALSE);
		$myPicture->setGraphArea(60,50,1065,760);
		$myPicture->setFontProperties(["Color"=> new pColor(0),"FontName"=>"fonts/Cairo-Regular.ttf","FontSize"=>22]);

		$Settings = ['Pos'=>SCALE_POS_LEFTRIGHT, 'Mode'=>SCALE_MODE_FLOATING, 'LabelingMethod'=>LABELING_ALL, 'GridColor'=>new pColor(255,255,255,50), 'TickColor'=>new pColor(0,0,0,50), 'LabelRotation'=>0, 'CycleBackground'=>DIRECTION_VERTICAL, 'DrawXLines'=>VOID, 'DrawSubTicks'=>DIRECTION_VERTICAL, 'SubTickColor'=>new pColor(255,0,0,50), 'DrawYLines'=>NONE];
		$myPicture->drawScale($Settings);

		$myPicture->setShadow(TRUE,["X"=>1,"Y"=>1,"Color"=>new pColor(50,50,50,10)]);

		$Config = ['ForceTransparency'=>50, 'AroundZero'=>DIRECTION_VERTICAL];
		(new pCharts($myPicture))->drawFilledSplineChart($Config);

		$myPicture->stroke();
		
	}
   
?>