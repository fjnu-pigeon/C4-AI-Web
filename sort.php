<?php
    require 'conn.php';
    //查询数据表中的数据
    $sql = "select * from classes";
    $result = $con->query($sql); 
    // $datarow = mysqli_num_rows($sql); //长度
    // //循环遍历出数据表中的数据
    // for($i=0;$i<$datarow;$i++){
    //     $sql_arr = mysqli_fetch_assoc($sql);
        // $id = $sql_arr['Building'];
        // $Classes = $sql_arr['Classes'];
        // $MaxNum = $sql_arr['MaxNum'];
        // $Num = $sql_arr['num'];
        // $Percent=$sql_arr['Percentage'];
        // echo "$id";
    // }
    $a = array();
    while($row = $result->fetch_assoc()) { 
        $count=count($row);//不能在循环语句中，由于每次删除row数组长度都减小 
        for($i=0;$i<$count;$i++){ 
          unset($row[$i]);//删除冗余数据 
        } 
        array_push($a,$row); 
        
    } 
    // header("content-type:application/json");
    $j=json_encode($a);
    $array = json_decode($j,TRUE);
    // var_dump($array[0]["Building"]);

    if (!function_exists('array_column')) {
        function array_column($arr2, $column_key) {
            $data = array();
            foreach ($arr2 as $key => $value) {
                $data[] = $value[$column_key];
            }
            return $data;
        }
    }
    $per=array_column($array,'Percentage');
    array_multisort($per,$array);
    // var_dump($array);
?>