# 基于飞桨的高校人流量识别和查询系统-网页端
[![star](https://gitee.com/fjnu-pigeon/C4-AI-Web/badge/star.svg?theme=dark)](https://gitee.com/fjnu-pigeon/C4-AI-Web/stargazers)
[![fork](https://gitee.com/fjnu-pigeon/C4-AI-Web/badge/fork.svg?theme=dark)](https://gitee.com/fjnu-pigeon/C4-AI-Web/members)

## 介绍
[基于飞桨的高校人流量识别和查询系统](https://gitee.com/fjnu-pigeon/C4-AI)的网页客户端，可以通过网页查询对应的数据，降低使用成本

## 使用方法
1. 服务端  
克隆至本地，在PHP7的环境下即可顺利运行
2. 客户端  
像正常网页一样，输入网址即可访问😉

## 在线预览
[点击这里](https://www.fjnu-study.cn/)即可访问我们的网站🥳

## 截图预览
![截图1](https://gitee.com/fjnu-pigeon/C4-AI-Web/raw/test/snapshot/snapshot_1.png)
![截图2](https://gitee.com/fjnu-pigeon/C4-AI-Web/raw/test/snapshot/snapshot_2.png)
![截图3](https://gitee.com/fjnu-pigeon/C4-AI-Web/raw/test/snapshot/snapshot_3.png)
![截图4](https://gitee.com/fjnu-pigeon/C4-AI-Web/raw/test/snapshot/snapshot_4.png)


## 开源协议
[Apache License 2.0](./LICENSE)




