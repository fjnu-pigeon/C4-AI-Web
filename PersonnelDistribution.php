<!DOCTYPE html>
<html lang="zn-ch">
<head>
    <!--Meta Informations-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=0.5,user-scalable=no">

    <!--Site Title-->
    <title>人员分布图</title>


    <!--Style Assets-->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl/owl.carousel.css">


    <!--Theme Style-->
    <link rel="stylesheet" href="assets/css/styles.css">
    <script>
        function IsPC() {
            var userAgentInfo = navigator.userAgent;
            var Agents = ["Android", "iPhone",
                "SymbianOS", "Windows Phone",
                "iPad", "iPod"];
            var flag = true;
            for (var v = 0; v < Agents.length; v++) {
                if (userAgentInfo.indexOf(Agents[v]) > 0) {
                    flag = false;
                    break;
                }
            }
            return flag;
        }

        if (IsPC()) {
            var link = document.createElement('link');
            link.type = 'text/css';
            link.rel = 'stylesheet';
            link.href = "assets/css/table_pc.css";
            var head = document.getElementsByTagName('head')[0];
            head.appendChild(link);

        } else {
            var link = document.createElement('link');
            link.type = 'text/css';
            link.rel = 'stylesheet';
            link.href = "assets/css/table_phone.css";
            var head = document.getElementsByTagName('head')[0];
            head.appendChild(link);
        }
    </script>


    <script>
        function showResult(str) {
            if (str.length == 0) {
                document.getElementById("livesearch").innerHTML = "";
                document.getElementById("livesearch").style.border = "0px";
                return;
            }
            if (window.XMLHttpRequest) {// IE7+, Firefox, Chrome, Opera, Safari 浏览器执行
                xmlhttp = new XMLHttpRequest();
            } else {// IE6, IE5 浏览器执行
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("livesearch").innerHTML = xmlhttp.responseText;
                    //  document.getElementById("livesearch").style.border="1px solid #A5ACB2";
                }
            }
            str = encodeURI(str);
            var url = "livesearch.php?q=" + str;
            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        }
    </script>


</head>
<body>

<div id="page" class="site row">

    <header id="header" class="site-header">

        <nav class="navbar navbar-defatult navbar-fixed-top fluid-navbar navbar-style1">
            <div class="container-fluid">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav"
                            aria-expanded="false">
                        <span class="sr-only">Nav Opener</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="main-nav">

                    <ul class="navbar-nav nav navbar-right">
                        <li ><a href="index.php">首页</a></li>
                        <li class="active"><a href="recommended.php">推荐</a></li>
                        <li><a href="all.php">楼层</a></li>
                        <li><a href="https://cloud.fjnu-study.cn/index.php/s/9qgsLiKMSPT5ZST">下载</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    </header>

    <main id="contents" class="site-contnts">

        <!--Slider-->
        <div class="home-slider">
            <div class="item" data-slide="assets/images/wk.jpg">

            </div>
            <div class="item" data-slide="assets/images/qs.jpg">


            </div>
        </div>

        <form class="row domain-search bg-pblue" action="search.php" method="POST">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h2 class="form-title">寻找您<strong>喜欢的教室</strong></h2>
                        <p>Search for your dream classroom</p>
                    </div>
                    <div class="col-md-9">
                        <div class="input-group">
                            <input type="text" class="form-control" name="s" placeholder="search" id="txt_ide"
                                   list="ide" onkeyup="showResult(this.value)">
                            <div id="livesearch"></div>
                            <span class="input-group-addon"><input type="submit" value="Search" class="btn btn-primary"></span>
                        </div>
                    </div>
                </div>
            </div>
        </form>


        <section class="row blog-section">
            <div class="container">
                <div class="row section-title text-center">
                 <?php
                    $build = $_GET['build'];
                    $class = $_GET['class'];
                    $b = '123';
                    if ($build == 'LC') {
                        $b = '立诚';
                    } elseif ($build == 'ZM') {
                        $b = '知明';
                    } elseif ($build == 'XD') {
                        $b = '笃行';
                    } elseif ($build == 'ZG') {
                        $b = '致广';
                    }
                    echo '<h2>'.$b.' '.$class.' 人员分布图'.'</h2>';
                    ?>  

                </div>
                <?php
                echo
                    '<p style="text-align: center;">
                        <img src="https://www.fjnu-study.cn/request/tempImg/pic_distribution.php?build=' . $build . '&&class=' . $class . '" width="80%">
                    </p>';
                
                echo
                    '<p style="text-align: center;">
                        <img src="assets/images/2.jpg" width="70%">
                    </p>';
                ?>
            </div>
    </main>

    <!--Footer-->
    <footer id="footer" class="site-footer">
        <!--Footer Top-->
        <section class="row site-footer-top">

            <div class="container">
                <div class="row">
                    <div class="media">

                        <div class="col-sm-9 media-body">
                            <p>All Rights Reserved </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="vendors/jquery-2.2.0.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/owl/owl.carousel.min.js"></script>
        <script src="vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="vendors/isotope.pkgd.min.js"></script>
        <script src="vendors/imagesloaded.pkgd.min.js"></script>
        <!--Theme JS-->
        <script src="assets/js/hostpro.js"></script>
</body>
</html>