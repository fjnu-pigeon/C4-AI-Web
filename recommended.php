<!DOCTYPE html>
<html lang="zn-ch">
<head>
    <!--Meta Informations-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=0.5,user-scalable=no">

    <!--Site Title-->
    <title>推荐座位</title>


    <!--Style Assets-->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl/owl.carousel.css">


    <!--Theme Style-->
    <link rel="stylesheet" href="assets/css/styles.css">
    <script>
        function IsPC() {
            var userAgentInfo = navigator.userAgent;
            var Agents = ["Android", "iPhone",
                "SymbianOS", "Windows Phone",
                "iPad", "iPod"];
            var flag = true;
            for (var v = 0; v < Agents.length; v++) {
                if (userAgentInfo.indexOf(Agents[v]) > 0) {
                    flag = false;
                    break;
                }
            }
            return flag;
        }

        if (IsPC()) {
            var link = document.createElement('link');
            link.type = 'text/css';
            link.rel = 'stylesheet';
            link.href = "assets/css/table_pc.css";
            var head = document.getElementsByTagName('head')[0];
            head.appendChild(link);

        } else {
            var link = document.createElement('link');
            link.type = 'text/css';
            link.rel = 'stylesheet';
            link.href = "assets/css/table_phone.css";
            var head = document.getElementsByTagName('head')[0];
            head.appendChild(link);
        }
    </script>


    <script>
        function showResult(str) {
            if (str.length == 0) {
                document.getElementById("livesearch").innerHTML = "";
                document.getElementById("livesearch").style.border = "0px";
                return;
            }
            if (window.XMLHttpRequest) {// IE7+, Firefox, Chrome, Opera, Safari 浏览器执行
                xmlhttp = new XMLHttpRequest();
            } else {// IE6, IE5 浏览器执行
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("livesearch").innerHTML = xmlhttp.responseText;
                    //  document.getElementById("livesearch").style.border="1px solid #A5ACB2";
                }
            }
            str = encodeURI(str);
            var url = "livesearch.php?q=" + str;
            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        }
    </script>


</head>
<body>

<div id="page" class="site row">

    <header id="header" class="site-header">

        <nav class="navbar navbar-defatult navbar-fixed-top fluid-navbar navbar-style1">
            <div class="container-fluid">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav"
                            aria-expanded="false">
                        <span class="sr-only">Nav Opener</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="main-nav">

                    <ul class="navbar-nav nav navbar-right">
                        <li ><a href="index.php">首页</a></li>
                        <li class="active"><a href="recommended.php">推荐</a></li>
                        <li><a href="all.php">楼层</a></li>
                        <li><a href="https://cloud.fjnu-study.cn/index.php/s/9qgsLiKMSPT5ZST">下载</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    </header>

    <main id="contents" class="site-contnts">

        <!--Slider-->
        <div class="home-slider">
            <div class="item" data-slide="assets/images/wk.jpg">

            </div>
            <div class="item" data-slide="assets/images/qs.jpg">


            </div>
        </div>

        <form class="row domain-search bg-pblue" action="search.php" method="POST">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h2 class="form-title">寻找您<strong>喜欢的教室</strong></h2>
                        <p>Search for your dream classroom</p>
                    </div>
                    <div class="col-md-9">
                        <div class="input-group">
                            <input type="text" class="form-control" name="s" placeholder="search" id="txt_ide"
                                   list="ide" onkeyup="showResult(this.value)">
                            <div id="livesearch"></div>
                            <span class="input-group-addon"><input type="submit" value="Search" class="btn btn-primary"></span>
                        </div>
                    </div>
                </div>
            </div>
        </form>


        <section class="row blog-section">
            <div class="container">
                <div class="row section-title text-center">
                    <h2>推荐座位</h2>

                </div>
                <div class="row row loop-post single-post">

                    <div class="col-sm-3 text-center">
                        <div class="row text-center">
                            <table style='text-align:left;' border='1'>
                                <tr>
                                    <th>楼层</th>
                                    <th>教室号</th>
                                    <th>使用率</th>
                                    <th>当前剩余座位数</th>
                                    <th>最大座位</th>
                                    <th>人数变化信息</th>
                                    <th>教室详细信息</th>
                                </tr>
                                <?php
                                //引用conn.php文件
                                require 'sort.php';
                                //查询数据表中的数据
                                //循环遍历出数据表中的数据

                                for ($i = 0; $i < count($array) && $array[$i]['Percentage'] <= 50; $i++) {
                                    $b = $array[$i]['Building'];
                                    if ($b == 'LC') {
                                        $b = '立诚';
                                    } elseif ($b == 'ZM') {
                                        $b = '知明';
                                    } elseif ($b == 'XD') {
                                        $b = '笃行';
                                    } elseif ($b == 'ZG') {
                                        $b = '致广';
                                    }
                                    $c = $array[$i]['Classes'];
                                    $m = $array[$i]['MaxNum'];
                                    $n = $array[$i]['num'];
                                    $p = $array[$i]['Percentage'];
                                    $left = $m - $n;


                                    $type = "OneClass";
                                    $build = $array[$i]['Building'];
                                    $class = $array[$i]['Classes'];
                                    if ($p > 50) {
                                        echo '<tr><td>' . $b . '</td><td>' . $c . '</td><td><font color="red">' . $p . '%</td><td>' . $left . '</td><td>' . $m . '</td><td><a href="https://www.fjnu-study.cn/PeopleChange.php?build=' . $build . '&&class=' . $class . '" style="color:#7e0001">查看人数变化图片</a></td><td><a href="https://www.fjnu-study.cn/PersonnelDistribution.php?build=' . $build . '&&class=' . $class . '" style="color:#7e0001">查看人员分布图</a></td></tr>';
                                    } else {
                                        echo '<tr><td>' . $b . '</td><td>' . $c . '</td><td><font color="#009900">' . $p . '%</td><td>' . $left . '</td><td>' . $m . '</td><td><a href="https://www.fjnu-study.cn/PeopleChange.php?build=' . $build . '&&class=' . $class . '"  style="color:#7e0001">查看人数变化图片</a></td><td><a href="https://www.fjnu-study.cn/PersonnelDistribution.php?build=' . $build . '&&class=' . $class . '" style="color:#7e0001">查看人员分布图</a></td></tr>';
                                    }

                                }

                                // echo $array[0]["Building"];
                                ?>
                            </table>

                        </div>
                    </div>


                </div>
            </div>
        </section>


        <section class="row client-logos">
            <div class="container">
                <div class="row section-title text-center">
                    <h2>楼群</h2>
                </div>
                <div class="row">
                    <div class="col-sm-3 client-logo"><a href="Cls.php?build=ZM" data-toggle="tooltip"
                                                         data-placement="top" title="知明楼"><img
                                    src="assets/images/zm.png" alt="知明楼"></a></div>
                    <div class="col-sm-3 client-logo"><a href="Cls.php?build=XD" data-toggle="tooltip"
                                                         data-placement="top" title="笃行楼"><img
                                    src="assets/images/dx.png" alt="笃行楼"></a></div>
                    <div class="col-sm-3 client-logo"><a href="Cls.php?build=LC" data-toggle="tooltip"
                                                         data-placement="top" title="立诚楼"><img
                                    src="assets/images/lc.png" alt="立诚楼"></a></div>
                    <div class="col-sm-3 client-logo"><a href="Cls.php?build=ZG" data-toggle="tooltip"
                                                         data-placement="top" title="致广楼"><img
                                    src="assets/images/zg.png" alt="致广楼"></a></div>
                </div>
            </div>
        </section>

    </main>

    <!--Footer-->
    <footer id="footer" class="site-footer">
        <!--Footer Top-->
        <section class="row site-footer-top">

            <div class="container">
                <div class="row">
                    <div class="media">

                        <div class="col-sm-9 media-body">
                            <p>All Rights Reserved </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="vendors/jquery-2.2.0.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/owl/owl.carousel.min.js"></script>
        <script src="vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="vendors/isotope.pkgd.min.js"></script>
        <script src="vendors/imagesloaded.pkgd.min.js"></script>
        <!--Theme JS-->
        <script src="assets/js/hostpro.js"></script>
</body>
</html>